<?php

session_start();

// Path to content file
define("JSON_PATH", "../../etc/resources/navigator_contents.json");

// User must be logged in or send their credentials via a Basic Auth header
if (isset($_SESSION["currentUser"]) || authenticateUser()) {

    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri = explode('/', $uri);

    // Filter out every part of the url before the actual endpoints and params
    while ($uri[0] != "www-navigator-api.php") {
        array_shift($uri);
    }
    array_shift($uri);

    $body = json_decode(file_get_contents('php://input'), true);


    switch (sizeof($uri)) {

        case 2:
            if (strtolower($uri[1] == "subtopics")) {

                switch ($_SERVER["REQUEST_METHOD"]) {

                        // GET /{topic}/subtopics
                    case "GET":
                        getSubtopics($uri[0]);

                        // POST /{topic}/subtopics
                    case "POST":
                        postSubtopic($uri[0], strtolower($body["subTopic"]));

                    default:
                        notFound();
                }
            } else {
                notFound();
            }


        case 3:
            switch (strtolower($uri[2])) {

                case "content":
                    switch ($_SERVER["REQUEST_METHOD"]) {

                            // GET /{topic}/{subtopic}/content
                        case "GET":
                            getContent($uri[0], $uri[1]);

                            // POST /{topic}/{subtopic}/content
                        case "POST":
                            postContent($uri[0], $uri[1], $body["content"]);

                        default:
                            notFound();
                    }

                case "references":
                    switch ($_SERVER["REQUEST_METHOD"]) {

                            // GET /{topic}/{subtopic}/references
                        case "GET":
                            getReferences($uri[0], $uri[1]);

                            // POST /{topic}/{subtopic}/references
                        case "POST":
                            postReferences($uri[0], $uri[1], $body["reference"]);

                        default:
                            notFound();
                    }

                default: {
                        notFound();
                    }
            }
        default:
            notFound();
    }
} else {
    http_response_code(401);
    die('401 Unauthorized');
}


function getJSON()
{
    return json_decode(file_get_contents((JSON_PATH)), true);
}

function getSubtopics($topic)
{
    $responseData = json_encode(array_keys(getJSON()[$topic]));

    header("Content-Type: application/json");
    echo $responseData;
    exit();
}

function postSubTopic($topic, $subTopic)
{
    validateString($subTopic);
    $data = getJSON();

    if (!isset($data[$topic][$subTopic])) {
        $data[$topic][$subTopic] = [
            "content" => [
                "text" => "Dieses Feld kann mit interessantem Inhalt gefüllt werden!",
                "lastModifiedTime" => "",
                "lastModifiedBy" => "",
                "md5" => ""
            ],
            "references" => [],
        ];
        file_put_contents(JSON_PATH, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        $responseData = json_encode(array_keys($data[$topic]));

        header("Content-Type: application/json");
        echo $responseData;
        exit();
    } else {
        header("HTTP/1.1 422 Unprocessable Entity");
        exit();
    }
}

function getContent($topic, $subTopic)
{
    $responseData = json_encode(getJSON()[$topic][$subTopic]["content"]);

    header("Content-Type: application/json");
    echo $responseData;
    exit();
}

function postContent($topic, $subTopic, $content)
{
    validateString($content["text"]);
    $data = getJSON();

    // Check if text has been changed while editing
    if ($data[$topic][$subTopic]["content"]["md5"] == $content["oldMd5"]) {

        $currentDateTime = date("d/m/Y H:i:s");

        $data[$topic][$subTopic]["content"]["text"] = $content["text"];
        $data[$topic][$subTopic]["content"]["lastModifiedTime"] = $currentDateTime;
        $data[$topic][$subTopic]["content"]["lastModifiedBy"] = $_SESSION["currentUser"];
        $data[$topic][$subTopic]["content"]["md5"] = md5($currentDateTime . $_SESSION["currentUser"]);
        file_put_contents(JSON_PATH, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        $responseData = json_encode($data[$topic][$subTopic]["content"]);

        header("Content-Type: application/json");
        echo $responseData;
        exit();
    } else {
        header("HTTP/1.1 409 Conflict");
        exit();
    }
}

function getReferences($topic, $subTopic)
{
    $responseData = json_encode(getJSON()[$topic][$subTopic]["references"]);

    header("Content-Type: application/json");
    echo $responseData;
    exit();
}

function postReferences($topic, $subTopic, $reference)
{
    validateString($reference);
    $data = getJSON();
    array_push($data[$topic][$subTopic]["references"], $reference);
    file_put_contents(JSON_PATH, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    $responseData = json_encode($data[$topic][$subTopic]["references"]);

    header("Content-Type: application/json");
    echo $responseData;
    exit();
}

function notFound()
{
    header("HTTP/1.1 404 Not Found");
    exit();
}

function authenticateUser()
{
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
        include '../../etc/salt.php';
        $username = hash("sha384", $_SERVER['PHP_AUTH_USER'] . $SALT);
        $password = hash("sha384", $_SERVER['PHP_AUTH_PW'] . $SALT);
        if (file_exists('../../etc/accounts/' . $username)
            && file_get_contents('../../etc/accounts/' . $username) == $password) {
                $_SESSION["currentUser"] = $_SERVER['PHP_AUTH_USER'];
                return true;
            } else return false;
    } else return false;
}

//Validation if POSTed value is just a string as expected or something that would break the content-JSON file
function validateString($value)
{
    if (is_string($value)) {
        return true;
    } else {
        header("HTTP/1.1 400 Bad Request");
        exit();
    }
}
