<?php
session_start();

if (!isset($_SESSION["currentUser"])) {
    header("Location: ./index.php");
    exit;
}

function logout()
{
    session_destroy();
    echo "<script>window.location.href = './index.php'</script>";
}

if (array_key_exists('logout', $_POST)) {
    logout();
}
?>