<!-- Template by W3Schools
 https://www.w3schools.com/howto/howto_js_topnav_responsive.asp-->


<style>
    nav {
        display: flex;
        justify-content: space-between;
        overflow: hidden;
        background-color: #333;
    }

    nav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 10px 16px;
        text-decoration: none;
        font-size: 17px;
    }

    nav a:hover {
        background-color: #ddd;
        color: black;
    }

    nav a.active {
        background-color: #808080;
        color: white;
    }

    #logout-area {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    #logout-area>form {
        padding-left: 1em;
        padding-right: 1em;
    }

    #username {
        font-size: 17px;
        color: #f2f2f2;
        text-decoration: none;
    }

    /*From https://fdossena.com/?p=html5cool/buttons/i.frag*/
    #logoutButton {
        padding: 0.35em 1.2em;
        border: 0.1em solid #FFFFFF;
        border-radius: 0.12em;
        box-sizing: border-box;
        text-decoration: none;
        font-weight: 300;
        color: #FFFFFF;
        text-align: center;
        transition: all 0.2s;
        background-color: rgba(0, 0, 0, 0);
    }

    #logoutButton:hover {
        color: #000000;
        background-color: #FFFFFF;
    }

    nav .icon {
        display: none;
    }

    /* For mobile phones */
    @media screen and (max-width: 600px) {

        .topnav a:not(:first-child) {
            display: none;
        }

        nav a.icon {
            float: right;
            display: block;
        }

        .topnav.responsive {
            position: relative;
        }

        .topnav.responsive .icon {
            position: absolute;
            left: 0;
            top: 0;
        }

        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }

        #username {
            display: none;
        }

        #logout-area {
            align-self: flex-start;
        }

        #logout-area>form {
            padding: 8px;
        }
    }
</style>

<nav>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
    </a>
    <div class="topnav" id="link-area">
        <a id="www-navigator-nav" href="./www-navigator.php"><i class="fas fa-home"></i> WWW-Navigator</a>
        <a id="rednerliste-nav" href="./rednerliste.php">Rednerliste</a>
        <a id="topsort-nav" href="./topsort.php">Topsort</a>
        <a id="textanalyse-nav" href="./textanalyse.php">Textanalyse</a>
        <a id="dom-performance-nav" href="./dom-performance.php">DOM-Performance</a>

    </div>

    <div id="logout-area">
        <div id="username">Benutzer: <?php echo $_SESSION["currentUser"]; ?> </div>
        <form method="post">
            <input type="submit" id="logoutButton" name="logout" value="Abmelden">
        </form>
    </div>
</nav>

<script>
    function myFunction() {
        var x = document.getElementById("link-area");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>