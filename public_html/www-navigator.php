<?php include "./session-header.php" ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WWW-Navigator</title>
    <script src="https://kit.fontawesome.com/3f12a5c9c5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./styles/global-styles.css">
    <link rel="stylesheet" href="./styles/www-navigator.css">
    <script src="https://vuejs.org/js/vue.min.js"></script>
    <script src="./vue/component-management.js" defer></script>
    <script src="./scripts/www-navigator.js" defer></script>

    <?php include "./vue/modal-dialog-component.html" ?>

</head>

<body>
    <?php include "./navigation-bar.php" ?>
    <div id="app">
        <header>
            <h1>WWW-Navigator</h1>

            <!-- For desktop resolutions -->
            <ul id="topic-list">
                <li id="html-button">
                    <h2>HTML</h2>
                </li>
                <li id="css-button">
                    <h2>CSS</h2>
                </li>
                <li id="javascript-button">
                    <h2>JavaScript</h2>
                </li>
            </ul>

            <!-- For mobile phone resolutions -->
            <select name="topics" id="topic-dropdown">
                <option value="" disabled selected hidden>Wählen Sie ein Thema</option>
                <option id="html-option" value="html">HTML</option>
                <option id="css-option" value="css">CSS</option>
                <option id="javascript-option" value="javascript">JavaScript</option>
            </select>
        </header>

        <div id="left-bar" class="sidebar">
            <!-- For desktop resolutions -->
            <ul id="subtopic-list"></ul>

            <!-- For mobile phone resolutions -->
            <select name="subtopics" id="subtopic-dropdown" class="hidden">
            </select>

            <i id="addSubTopicButton" class="fas fa-plus fa-2x hidden" @click="showAddSubTopicModal = true"></i>
            <modal v-if="showAddSubTopicModal" @add="addSubTopic" @cancel="showAddSubTopicModal = false" title="Unterthema hinzufügen"></modal>
        </div>

        <main id="content">
            <div id="contentText"></div>
        </main>

        <div id="right-bar" class="sidebar">
            <h3 id="referenceheader"></h3>
            <ul id="references"></ul>
            <i id="addReferenceButton" class="fas fa-plus fa-2x hidden" @click="showReferenceModal = true"></i>
            <modal v-if="showReferenceModal" @add="addReference" @cancel="showReferenceModal = false" title="Referenz hinzufügen"></modal>
        </div>

    </div>
</body>

</html>