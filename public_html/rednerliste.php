<?php include "./session-header.php" ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rednerliste</title>
    <script src="https://kit.fontawesome.com/3f12a5c9c5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./styles/global-styles.css">
    <link rel="stylesheet" href="./styles/rednerliste.css">
    <script src="./scripts/rednerliste.js" defer></script>
</head>

<body>
    <?php include "./navigation-bar.php" ?>

    <div class="flex-container">
        <div id="app">
            <h1>Rednerliste</h1>

            <label for="rednerInput">Neuer Redner:</label>
            <input id="rednerInput" type="text">
            <button id="addButton" onclick="addRedner()">Hinzufügen</button>

            <br>

            <ul id="rednerList"></ul>
        </div>
    </div>

</body>

</html>