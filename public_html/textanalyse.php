<?php include "./session-header.php" ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/3f12a5c9c5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./styles/global-styles.css">
    <title>Textanalyse</title>
    <script src="./scripts/textanalyse.js" defer></script>
</head>

<body>
    <?php include "./navigation-bar.php" ?>

    <div class="flex-container">
        <div id="app">

            <h1>Textanalyse</h1>
            <textarea id="textInput" cols="30" rows="10"></textarea>
            <br>
            <label for="stopWordsInput">Zu ignorierende Wörter (kommasepariert):</label>
            <br>
            <input id="stopWordsInput" type="text" value="der, die, das">
            <button id="analyzeButton">Analysieren</button>

            <ul class="hidden" id="resultList">
                <li>
                    Platz 1: <b><span id="topWord1"></span></b>, kommt <b><span id="topWord1Count"></span></b> mal vor.
                </li>
                <li>
                    Platz 2: <b><span id="topWord2"></span></b>, kommt <b><span id="topWord2Count"></span></b> mal vor.
                </li>
                <li>
                    Platz 3: <b><span id="topWord3"></span></b>, kommt <b><span id="topWord3Count"></span></b> mal vor.
                </li>
            </ul>
            
        </div>
    </div>
</body>

</html>