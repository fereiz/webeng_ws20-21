<?php
session_start();

define("LOGIN_PATH", "./login.php");
define("MAIN_PATH", "./www-navigator.php");

if (!isset($_SESSION["currentUser"])) {
    header("Location: " . LOGIN_PATH);
} else {
    header("Location: " . MAIN_PATH);
}
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Engineering WS20/21</title>
</head>

<body>

</body>

</html>