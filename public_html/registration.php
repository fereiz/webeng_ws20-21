<?php
session_start();

if (isset($_SESSION["currentUser"])) {
    header("Location: ./www-navigator.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="de">

<?php
include '../etc/salt.php';

if (isset($_POST['name']) && isset($_POST['password'])) {
    if (!empty($_POST['name']) && !empty($_POST['password'])) {
        $username = hash("sha384", $_POST['name'] . $SALT);
        $passwd = hash("sha384", $_POST['password'] . $SALT);

        $account_dir = '../etc/accounts/';

        if (!file_exists($account_dir . $username)) {
            $new_file = fopen($account_dir . $username, "w");
            fwrite($new_file, $passwd);
            fclose($new_file);
            echo "<script>alert('Erfolgreich registriert!')</script>";
            echo "<script>window.location.href = './login.php';</script>";
        } else {
            echo "<script>alert('Nutzer existiert bereits.')</script>";
        }
    } else {
        echo "<script>alert('Bitte Nutzername und Passwort eingeben!')</script>";
    }
}
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/global-styles.css">
    <link rel="stylesheet" href="./styles/auth.css">
    <title>Registrierung</title>
</head>

<body>
    <div id="app">
        <div id="authBox">
            <h1>Registrierung</h1>
            <form method="post">
                <label for="nameInput">Name:</label>
                <br>
                <input id="nameInput" type="text" name="name">
                <br>
                <br>
                <label for="passwordInput">Passwort:</label>
                <br>
                <input id="passwordInput" type="password" name="password">
                <br> <br>
                <input id="submitButton" type="submit" value="Registrieren">
            </form>
            <p>Schon registriert? <a href="./index.php"><br>Hier geht's zum Login!</a></p>
        </div>
    </div>
</body>

</html>