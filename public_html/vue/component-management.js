Vue.component("modal", {
    props: ['title'],
    template: "#modal-template",
    data: function () {
        return {
            input: ""
        }
    }
});

new Vue({
    el: "#app",
    data: {
        showAddSubTopicModal: false,
        showReferenceModal: false
    },
    methods: {
        addSubTopic(subTopic) {
            if (!window.editMode || window.confirmChangeDiscard()) {
                const activeTopicName = window.activeTopic.textContent.toLowerCase();
                window.addSubTopic(activeTopicName, subTopic).then(response => { 
                    window.populateSubTopics(activeTopicName, response);
                    window.selectSubTopic(activeTopicName, subTopic);
                    this.showAddSubTopicModal = false;
                }).catch(error => {
                    alert(error.message);
                });
            }
        },
        addReference(reference) {
            const activeTopicName = window.activeTopic.textContent.toLowerCase();
            const activeSubTopicName = window.activeSubTopic.textContent.toLowerCase()
            window.addReference(activeTopicName, activeSubTopicName, reference).then(response => {
                window.clearReferences();
                window.fillReferences(response);
            });
            this.showReferenceModal = false;
        }
    }
});