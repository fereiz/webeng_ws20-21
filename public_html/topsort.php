<?php include "./session-header.php" ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/3f12a5c9c5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./styles/global-styles.css">
    <title>Topsort</title>
    <script src="./scripts/topsort.js" defer></script>
</head>

<body>
    <?php include "./navigation-bar.php" ?>

    <div class="flex-container">
        <div id="app">
            <h1>Topsort</h1>

            <ul id="dependencyList">
                <li id="dependency1">
                    <input id="input1A" type="text"> vor <input id="input1B" type="text">.
                </li>
                <br>
                <li id="dependency2">
                    <input id="input2A" type="text"> vor <input id="input2B" type="text">.
                </li>
                <br>
            </ul>

            <button id="newDependencyButton">Neue Abhängigkeit</button>
            <button id="deleteDependencyButton">Abhängigkeit entfernen</button>
            <button id="sortButton">Sortieren!</button>
            <br><br>
            <div id="output"></div>
        </div>
    </div>
</body>

</html>