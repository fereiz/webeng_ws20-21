
var activeTopic;
var activeSubTopic;
var editMode = false;

document.getElementById("www-navigator-nav").classList.add("active");

//Provide Header buttons with functionality
const topics = ["html", "css", "javascript"];
for (topic of topics) {
    //IIFE for closure over topics
    (topic => {
        document.getElementById(`${topic}-button`).addEventListener("click", () => {
            selectTopic(topic);
        });
    })(topic);
}

// Make topic selectable with the mobile only dropdown menu
document.getElementById("topic-dropdown").addEventListener("change", () => {
    selectTopic(document.getElementById("topic-dropdown").value);
});

// Make subtopic selectable with the mobile only dropdown menu
document.getElementById("subtopic-dropdown").addEventListener("change", () => {
    selectSubTopic(activeTopic.textContent.toLowerCase(), document.getElementById("subtopic-dropdown").value);
});

function selectTopic(topic) {
    if (!editMode || confirmChangeDiscard()) {
        makeActiveTopic(document.getElementById(`${topic}-button`));
        getSubTopics(topic).then(data => {
            populateSubTopics(topic, data);
        }).catch(error => {
            alert(error.message);
        });
    }
}


function populateSubTopics(topic, subTopics) {
    clearContent();
    clearReferences();
    const list = document.getElementById("subtopic-list");
    list.innerHTML = "";
    const subTopicDropdown = document.getElementById("subtopic-dropdown");
    subTopicDropdown.innerHTML = '<option value="" disabled selected hidden>Wählen Sie ein Unterthema</option>';
    for (let subTopic of subTopics) {

        //For desktop buttons
        const newLi = document.createElement("li");
        newLi.id = `${topic}-${subTopic}-button`;
        newLi.textContent = subTopic;
        newLi.addEventListener("click", () => selectSubTopic(topic, subTopic));
        list.appendChild(newLi);

        //For mobile dropdown
        newOption = document.createElement("option");
        newOption.id = `${topic}-${subTopic}-option`;
        newOption.value = subTopic;
        newOption.text = subTopic;
        subTopicDropdown.add(newOption);
    }

    //Make add button and subtopic dropdown (only mobile) appear when subtopics are loaded
    document.getElementById("subtopic-dropdown").classList.remove("hidden");
    document.getElementById("addSubTopicButton").classList.remove("hidden");
}

function selectSubTopic(topic, subTopic) {
    if (!editMode || confirmChangeDiscard()) {
        clearContent();
        clearReferences();
        makeActiveSubTopic(subTopic);
        getContent(topic, subTopic).then(content => {
            fillContent(topic, subTopic, content);
        }).catch(error => {
            alert(error.message);
        });
        getReferences(topic, subTopic).then(references => {
            fillReferences(references);
        }).catch(error => {
            alert(error.message);
        });
    }
}


function fillContent(topic, subTopic, content) {
    document.getElementById("contentText").textContent = content["text"];

    const editButton = document.createElement("button");
    editButton.id = "editButton";
    editButton.textContent = "Text bearbeiten";
    editButton.addEventListener("click", () => makeContentEditable(topic, subTopic));
    document.getElementById("content").appendChild(editButton);

    if (content["lastModifiedTime"] && content["lastModifiedBy"]) {
        const lastModifiedInfo = document.createElement("p");
        lastModifiedInfo.innerHTML = `<i>Zuletzt bearbeitet</i> <b>${content["lastModifiedTime"]}</b> <i>von</i> <b>${content["lastModifiedBy"]}</b>.`;
        document.getElementById("content").appendChild(lastModifiedInfo);
    }
}

function fillReferences(references) {
    document.getElementById("referenceheader").textContent = "Zusätzliche Informationen";
    const referenceList = document.getElementById("references");
    for (let reference of references) {
        const newLi = document.createElement("li");
        const newA = document.createElement("a");
        newA.href = reference;
        newA.textContent = reference;
        newLi.appendChild(newA);
        referenceList.appendChild(newLi);
    }

    //Make add button appear when subtopics are loaded
    document.getElementById("addReferenceButton").classList.remove("hidden");
}

function clearContent() {
    document.getElementById("content").innerHTML = "<div id='contentText'></div>";
}

function clearReferences() {
    document.getElementById("referenceheader").textContent = "";
    document.getElementById("references").innerHTML = "";
    document.getElementById("addReferenceButton").classList.add("hidden");
}

function makeActiveTopic(element) {
    if (activeTopic) activeTopic.classList.remove("active");
    element.classList.add("active");
    activeTopic = element;
    document.getElementById(`${activeTopic.textContent.toLowerCase()}-option`).selected = true;
}

function makeActiveSubTopic(subTopic) {
    if (activeSubTopic) activeSubTopic.classList.remove("active");
    const activeTopicName = activeTopic.textContent.toLowerCase();
    const subTopicButton = document.getElementById(`${activeTopicName}-${subTopic}-button`);
    subTopicButton.classList.add("active");
    activeSubTopic = subTopicButton;
    document.getElementById(`${activeTopicName}-${subTopic}-option`).selected = true;
}

function makeContentEditable(topic, subTopic) {
    getContent(topic, subTopic).then(content => {
        editMode = true;
        const contentElement = document.getElementById("contentText");
        contentElement.contentEditable = "true";
        contentElement.classList.add("editMode");

        document.getElementById("editButton").remove();

        const cancelButton = document.createElement("button");
        cancelButton.id = "cancelButton";
        cancelButton.textContent = "Abbrechen";
        cancelButton.addEventListener("click", () => {
            editMode = false;
            clearContent();
            fillContent(topic, subTopic, content);
        });

        const saveButton = document.createElement("button");
        saveButton.id = "saveButton";
        saveButton.textContent = "Speichern";
        saveButton.addEventListener("click", () => overwriteContent(topic, subTopic, contentElement.textContent, content["md5"]).then(
            content => {
                editMode = false;
                clearContent();
                fillContent(topic, subTopic, content);
            }).catch(error => {
                alert(error.message);
            }));
        document.getElementById("content").appendChild(saveButton);
        document.getElementById("content").appendChild(cancelButton);
    }).catch(error => {
        alert(error.message);
    });
}

// When loading new content with unsaved changes pop a prompt
function confirmChangeDiscard() {
    if (window.confirm("Sie haben ungespeicherte Daten.\nMöchten sie wirklich fortfahren?")) {
        editMode = false;
        return true;
    } else {
        return false;
    }
}

// When leaving the site with unsaved changes pop a prompt
window.addEventListener("beforeunload", (event) => {
    if (editMode) {
        event.preventDefault();
        // For chrome
        event.returnValue = "";
    }
})

//REST functions
async function getSubTopics(topic) {
    return await fetch(`./api/www-navigator-api.php/${topic}/subtopics`, {
        method: "GET"
    }).then(response => checkResponse(response));
}

async function addSubTopic(topic, subTopic) {
    return await fetch(`./api/www-navigator-api.php/${topic}/subtopics`, {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: `{
            "subTopic": "${subTopic}"
        }`
    }).then(response => checkResponse(response));
}

async function getContent(topic, subTopic) {
    return await fetch(`./api/www-navigator-api.php/${topic}/${subTopic}/content`, {
        method: "GET"
    }).then(response => checkResponse(response));
}

async function overwriteContent(topic, subTopic, text, oldMd5) {
    return await fetch(`./api/www-navigator-api.php/${topic}/${subTopic}/content`, {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: `{ 
            "content":{
                "text": "${text}",
                "oldMd5": "${oldMd5}"
            }
        }`
    }).then(response => checkResponse(response));
}

async function getReferences(topic, subTopic) {
    return await fetch(`./api/www-navigator-api.php/${topic}/${subTopic}/references`, {
        method: "GET"
    }).then(response => checkResponse(response));
}

async function addReference(topic, subTopic, reference) {
    return await fetch(`./api/www-navigator-api.php/${topic}/${subTopic}/references`, {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: `{
            "reference": "${reference}"
        }`
    }).then(response => checkResponse(response));
}

function checkResponse(response) {
    if (response.ok) {
        return response.json();
    } else {
        switch (response.status) {

            case 409:
                throw new Error("Die von Ihnen bearbeitete Version des Textes wurde mittlerweile aktualisiert.\nSichern Sie Ihre Änderungen extern und laden Sie den Text erneut.");

            case 422:
                throw new Error("Dieses Unterthema existiert bereits.")

            default:
                throw new Error("Etwas ist schiefgelaufen.");
        }
    }
}