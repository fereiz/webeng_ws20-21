document.getElementById("dom-performance-nav").classList.add("active");

const operations = ["innerHTML", "innerText", "textContent", "outerHTML"];

document.getElementById("calculateButton").addEventListener("click", () => {
    const repCount = document.getElementById("repCountInput").value;
    if (repCount > 0) {
        schreibeZeitenInTabelle(berechneAlle(repCount));
    }
});

function berechnePerformance(operation, getElement, setElement) {
    const t0 = performance.now();
    const text = getElement[operation];
    setElement[operation] = text;
    return performance.now() - t0;
}

function berechneAlle(repCount) {
    reset();
    document.getElementById("testHTML").firstElementChild.innerHTML = document.getElementById("textInput").value;
    const getElement = document.getElementById("testHTML").firstElementChild;
    let zeiten = new Map();
    for (o of operations) {
        let durationSum = 0;
        for (let i = 0; i < repCount; i++) {
            durationSum = durationSum + berechnePerformance(o, getElement, document.getElementById(`${o}Set`).firstElementChild);
        }
        zeiten.set(o, durationSum / repCount);
    }
    return zeiten;
}

function schreibeZeitenInTabelle(zeiten) {
    for (o of operations) {
        document.getElementById(`${o}Zelle`).innerText = `${zeiten.get(o)} ms`;
    }
}

function reset() {
    document.getElementById("testHTML").innerHTML = "<div></div>";
    for (o of operations) {
        document.getElementById(`${o}Set`).innerHTML = "<div></div>";
    }
}
