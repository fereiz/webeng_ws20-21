document.getElementById("textanalyse-nav").classList.add("active");

document.getElementById("analyzeButton").addEventListener("click", () => {
    const text = document.getElementById("textInput").value;
    const stopwords = document.getElementById("stopWordsInput").value.replaceAll(" ", "").split(",").map(word => word.toLowerCase());
    const topWords = textanalyse(text, stopwords);
    for (let i = 0; i < 3; i++) {
        if (topWords[i]) {
            document.getElementById(`topWord${i + 1}`).textContent = topWords[i][0];
            document.getElementById(`topWord${i + 1}Count`).textContent = topWords[i][1];
        }
    }
    document.getElementById("resultList").classList.remove("hidden");
});

function textanalyse(text, stopwords) {
    words = text.replace(/ä/g, 'ae').replace(/ü/g, 'ue').replace(/ö/g, 'oe').split(/\W+/);
    return Object.entries(words.filter(word => !stopwords.includes(word.toLowerCase()))
        .reduce((map, word) => {
            map[word] = (map[word] || 0) + 1;
            return map;
        }, Object.create(null))).sort((a, b) => b[1] - a[1]).slice(0, 3);
}