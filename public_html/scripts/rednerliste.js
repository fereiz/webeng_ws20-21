document.getElementById("rednerliste-nav").classList.add("active");

let rednerMap = new Map();
let rednerList = document.getElementById("rednerList");
let activeRedner;

function Redner(name) {
    this.name = name;
    this.talkingTime = 0;
    this.isTalking = true;
    this.startedTalking = performance.now();
    this.calculateTalkingTime = function () {
        this.talkingTime = this.talkingTime + (performance.now() - this.startedTalking);
        this.startedTalking = performance.now();
        return this.talkingTime;
    }
    this.stopTalking = function () {
        this.isTalking = false;
        this.calculateTalkingTime();
    }
    this.startTalking = function () {
        this.isTalking = true;
        this.startedTalking = performance.now();
    }
}

function addRedner() {
    const name = document.getElementById("rednerInput").value.trim();
    if (name && !rednerMap.has(name)) {
        const redner = new Redner(name);
        let listElement = document.createElement("li");
        listElement.innerHTML = `${name} <p id="${name}Time" class="timers">00:00:00</p> <button id="${name}Button" onclick="toggleTalking('${name}')">Stopp!</button>`;
        rednerList.appendChild(listElement);
        rednerMap.set(name, redner);
        if (activeRedner) {
            toggleTalking(activeRedner.name)
        }
        activeRedner = redner;
        document.getElementById("rednerInput").value = "";
        updateTime();
    }
}

document.getElementById("rednerInput").addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("addButton").click();
    }
});

function toggleTalking(rednerName) {
    const redner = rednerMap.get(rednerName);
    if (redner.isTalking) {
        redner.stopTalking();
        document.getElementById(`${rednerName}Button`).textContent = "Start!";
        activeRedner = null;
    } else {
        if (activeRedner) {
            toggleTalking(activeRedner.name)
        }
        redner.startTalking();
        activeRedner = redner;
        document.getElementById(`${rednerName}Button`).textContent = "Stopp!";
    }
}

function updateTime() {
    setTimeout(() => {
        try {
            document.getElementById(`${activeRedner.name}Time`).textContent = milisecondsToTimeFormat(activeRedner.calculateTalkingTime());
        } catch (err) { };
        updateTime();
    }, 1000);
}

function milisecondsToTimeFormat(ms) {
    const hours = Math.floor(ms / 1000 / 60 / 60).toString().padStart(2, "0");
    const minutes = Math.floor((ms / 1000 / 60 / 60 - hours) * 60).toString().padStart(2, "0");
    const seconds = Math.floor(((ms / 1000 / 60 / 60 - hours) * 60 - minutes) * 60).toString().padStart(2, "0");
    return `${hours}:${minutes}:${seconds}`;
}
