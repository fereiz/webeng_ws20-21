document.getElementById("topsort-nav").classList.add("active");

let dependencyCount = 2;

document.getElementById("newDependencyButton").addEventListener("click", addDependencyInputs);
document.getElementById("deleteDependencyButton").addEventListener("click", deleteDependencyInputs);
document.getElementById("sortButton").addEventListener("click", startSorting);


function addDependencyInputs() {
    dependencyCount = dependencyCount + 1;
    const dList = document.getElementById("dependencyList");
    const newLi = document.createElement("li");
    newLi.id = `dependency${dependencyCount}`
    newLi.innerHTML = `<input id="input${dependencyCount}A" type="text"> vor <input id="input${dependencyCount}B" type="text">.<br><br>`;
    dList.appendChild(newLi);
}

function deleteDependencyInputs() {
    if (dependencyCount > 2) {
        const child = document.getElementById(`dependency${dependencyCount}`);
        document.getElementById("dependencyList").removeChild(child);
        dependencyCount = dependencyCount - 1;
    }
}

function startSorting() {
    let dependencyArray = [];
    const output = document.getElementById("output");
    let emptyField = false;
    for (let i = 1; i <= dependencyCount; i++) {
        const value1 = document.getElementById(`input${i}A`).value;
        const value2 = document.getElementById(`input${i}B`).value;

        if (value1 && value2) {
            dependencyArray.push([value1, value2]);
        } else {
            emptyField = true;
            break;
        }
    }
    if (!emptyField) {
        try {
            output.innerHTML = `<b>Ergebnissortierung:</b> ${topsort(dependencyArray)}`;
        } catch (cycleError) {
            output.innerHTML = "Fehler! Zyklus gefunden.".fontcolor("red");
        }
    } else {
        output.innerHTML = "Es darf keine Eingabe leer sein!".fontcolor("red");
    }
}

// Topsort logic
function node(id) {
    this.id = id;
    this.tempMark = false;
    this.permMark = false;
}

//add the string as a node to a list if it is unique
function addUniqueNode(list, elementAsString) {
    if (!(list.some(e => e.id === elementAsString))) {
        list.push(new node(elementAsString))
    }
}


function visit(nextNode, arr, L, S) {
    //node is a leaf
    if (nextNode.permMark === true) { return; }
    //dependencies are cyclical -> return error
    if (nextNode.tempMark === true) { throw cycleError; }

    //tempmark to detect cycles
    nextNode.tempMark = true;

    //visit all nodes that are dependent on current node 
    for (x of arr) {
        if (x[0] === nextNode.id) { visit(S.find(e => e.id === x[1]), arr, L, S) }
    }

    //current node is a leaf or all dependent nodes are already visited -> mark permanent and append to head of list
    nextNode.tempMark = false;
    nextNode.permMark = true;
    L.unshift(nextNode.id);
}

//topsort via depth-first search algorithm
function topsort(arr) {
    //capture all unique elements as node objects in a list
    S = [];
    for (x of arr) {
        addUniqueNode(S, x[0]);
        addUniqueNode(S, x[1]);
    }
    //apply visit function to each node
    L = [];
    while (true) {
        nextNode = S.find(e => e.permMark === false);
        if (nextNode === undefined) { break; }
        else { visit(nextNode, arr, L, S); }
    }
    //return potentially non unique topological sort order as list of strings
    return L;
}


