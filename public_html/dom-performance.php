<?php include "./session-header.php" ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/3f12a5c9c5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./styles/global-styles.css">
    <link rel="stylesheet" href="./styles/dom-performance.css">
    <title>DOM-Performance Vergleich</title>
    <script src="./scripts/dom-performance.js" defer></script>
</head>

<body>
    <?php include "./navigation-bar.php" ?>

    <div class="flex-container">
        <div id="app">

            <h1>Performancevergleich von DOM Lese- und Schreibeoperationen</h1>

            <label for="textInput">Testeingabe:</label>
            <br>
            <textarea id="textInput"></textarea>
            <br><br>
            <label for="repCountInput">Anzahl Wiederholungen:</label>
            <br>
            <input id="repCountInput" type="number" min="1" value="100">
            <br><br>
            <button id="calculateButton">Berechnen</button>

            <h2>Test-HTML</h2>
            <div id="testHTML"></div>

            <h2>innerHTML</h2>
            <div id="innerHTMLSet"></div>

            <h2>innerText</h2>
            <div id="innerTextSet"></div>

            <h2>textContent</h2>
            <div id="textContentSet"></div>

            <h2>outerHTML</h2>
            <div id="outerHTMLSet"></div>

            <h2>Performance</h2>
            <table id="tabelle">
                <tr>
                    <th>Operation</th>
                    <th>Laufzeit</th>
                </tr>
                <tr>
                    <td>innerHTML</td>
                    <td id="innerHTMLZelle"></td>
                </tr>
                <tr>
                    <td>innerText</td>
                    <td id="innerTextZelle"></td>
                </tr>
                <tr>
                    <td>textContent</td>
                    <td id="textContentZelle"></td>
                </tr>
                <tr>
                    <td>outerHTML</td>
                    <td id="outerHTMLZelle"></td>
                </tr>
            </table>
            
        </div>
    </div>

</body>

</html>