add = (x, y) => x + y;
mul = (x, y) => x * y;
double = (x) => x + x;
square = (x) => x * x;
alert = (x) => x;

identity_function = (arg) => () => arg;
console.assert(identity_function("test")() === "test");

addf = (x) => (y) => x + y;
console.assert(addf(2)(5) === 7);

applyf = (f) => (x) => (y) => f(x, y);
console.assert(applyf((x, y) => x + y)(5)(6) === 11);

curry = (f, x) => (y) => f(x, y);
console.assert(curry((x, y) => x + y, 3)(2) === 5);

inc1 = (x) => addf(x)(1);
console.assert(inc1(41) === 42);

inc2 = (x) => applyf((x, y) => x + y)(x)(1);
console.assert(inc2(41) === 42);

inc3 = (x) => curry((x, y) => x + y, 1)(x);
console.assert(inc3(41) === 42);


methodize = function (f) {
    return function (y) {
        return f(this, y);
    }
}

Number.prototype.add = methodize(add);
Number.prototype.mul = methodize(mul);
console.assert((3).add(4) === 7);
console.assert((3).mul(4) === 12);

demethodize = function (f) {
    return function (x, y) {
        return f.call(x, y);
    }
}

console.assert(demethodize(Number.prototype.add)(5, 6) === 11);

twice = (f) => (x) => f(x, x);

console.assert(twice(add)(11) === 22);

composeu = (f1, f2) => (x) => f2(f1(x));

console.assert(composeu(double, square)(3) === 36);

composeb = (f1, f2) => (x, y, z) => f2(f1(x, y), z);

console.assert(composeb(add, mul)(2, 3, 5) === 25);

once = (f) => {
    ranOnce = false;
    return (...args) => {
        if (ranOnce === false) {
            ranOnce = true;
            return f(...args);
        } else throw "Funktion bereits aufgerufen";
    }
}

add_once = once(add);
console.assert(add_once(2, 3) === 5);
try {
    add_once(2, 3)
} catch (error) {
    console.assert(error === "Funktion bereits aufgerufen");
}

function counterf(start) {
    return {
        current: start,
        inc: function () {
            return ++this.current
        },
        dec: function () {
            return --this.current
        }
    };
}

var counter = counterf(10);
console.assert(counter.inc() === 11);
console.assert(counter.dec() === 10);

function revocable(f) {
    return {
        revoked: false,
        invoke: function (...args) {
            if (this.revoked === true) {
                throw "Function revoked"
            } else return f(...args);
        },
        revoke: function () { this.revoked = true },
    };
}

temp = revocable(alert);
console.assert(temp.invoke(7) === 7);
temp.revoke();
try {
    temp.invoke(8);
} catch (error){
    console.assert(error === "Function revoked");
}

function vector() {
    var arr = [];
    return{
        get: (position) => arr[position],
        store: (position, value) => arr[position] = value,
        append: (value) => arr.push(value)
    };
}

my_vector = vector();
my_vector.append(7);
my_vector.store(1, 8);
console.assert(my_vector.get(0) === 7);
console.assert(my_vector.get(1) === 8);