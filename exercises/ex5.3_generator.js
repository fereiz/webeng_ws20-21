class Vorrang {

  constructor(vorrangPairs) {
    this.vorrangPairs = vorrangPairs;

    this.pairCount = vorrangPairs.length;

    //Gather unique activities
    this.activities = new Set([].concat(...vorrangPairs));

    //Set up an adjacency map for the "nodes"
    const adjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][0] === a) { arr.push(vorrangPairs[i][1]) }
      }
      adjMap.set(a, new Set(arr))
    }
    this.adjMap = adjMap;

    //Set up an inverse adjacency map for the "nodes"
    const invAdjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][1] === a) { arr.push(vorrangPairs[i][0]) }
      }
      invAdjMap.set(a, new Set(arr))
    }
    this.invAdjMap = invAdjMap;
  }

  //Generator function that iterates over the elements in topological order via Kahn's algo
  * topsortGenerator() {
    //Only on first time execution

    //Gather all nodes which have no incoming edges in array S    
    var S = [];
    var workingMap = new Map(this.adjMap);
    var workingInvMap = new Map(this.invAdjMap);
    for (var e of this.activities) {
      if (this.invAdjMap.get(e).size == 0) {
        S.push(e)
      }
    }
    //If graph is cyclical
    if (S.length == 0) { throw "No topological ordering possible" }

    while (S.length > 0) {
      //Remove a node n from S
      let n = S.pop();
      //Find all nodes m which have edges coming in from n and then
      for (const m of workingMap.get(n)) {
        //remove the edge
        workingMap.get(n).delete(m);
        workingInvMap.get(m).delete(n);
        //and insert m into S if it has no other incoming edges
        if (workingInvMap.get(m).size == 0) { S.push(m) }
      }
      //Return the removed node as long as there are nodes in S
      yield n;
    }
  }
}

const studentenLeben = new Vorrang([["schlafen", "studieren"], ["prüfen", "ausruhen"], ["essen", "studieren"], ["studieren", "prüfen"]]);

const gen = studentenLeben.topsortGenerator();

test1 = gen.next().value;
test2 = gen.next().value;
test3 = gen.next().value;
test4 = gen.next().value;
test5 = gen.next().value;

console.assert(("essen" === test1) || ("schlafen" === test1));
console.assert(("essen" === test2) || ("schlafen" === test2));
console.assert("studieren" === test3);
console.assert("prüfen" === test4);
console.assert("ausruhen" === test5);