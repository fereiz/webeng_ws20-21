class Vorrang {

  constructor(vorrangPairs) {
    this.vorrangPairs = vorrangPairs;

    this.pairCount = vorrangPairs.length;

    //Gather unique activities
    this.activities = new Set([].concat(...vorrangPairs));

    //Set up an adjacency map for the "nodes"
    const adjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][0] === a) { arr.push(vorrangPairs[i][1]) }
      }
      adjMap.set(a, new Set(arr))
    }
    this.adjMap = adjMap;

    //Set up an inverse adjacency map for the "nodes"
    const invAdjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][1] === a) { arr.push(vorrangPairs[i][0]) }
      }
      invAdjMap.set(a, new Set(arr))
    }
    this.invAdjMap = invAdjMap;
  }

  //Iterator that iterates over the elements in topological order via Kahn's algo
  [Symbol.iterator]() {
    //Gather all nodes which have no incoming edges in array S
    var S = [];
    var workingMap = new Map(this.adjMap);
    var workingInvMap = new Map(this.invAdjMap);
    for (var e of this.activities) {
      if (this.invAdjMap.get(e).size == 0) {
        S.push(e)
      }
    }
    //If graph is cyclical
    if (S.length == 0) { throw "No topological ordering possible" }

    var done = false;
    var n = NaN;
    return {
      next: () => {
        if (S.length > 0) {
          //Remove a node n from S
          n = S.pop();
          //Find all nodes m which have edges coming in from n and then
          for (const m of workingMap.get(n)) {
            //remove the edge
            workingMap.get(n).delete(m);
            workingInvMap.get(m).delete(n);
            //and insert m into S if it has no other incoming edges
            if (workingInvMap.get(m).size == 0) { S.push(m) }
          }
        } else done = true;
        //Return the removed node as long as there are nodes in S
        return {
          value: n,
          done: done
        }
      }
    }
  }
}

const studentenLeben = new Vorrang([["schlafen", "studieren"], ["prüfen", "ausruhen"], ["essen", "studieren"], ["studieren", "prüfen"]]);

var testArr = [];
for (const next of studentenLeben) {
  testArr.push(next);
}

console.assert(testArr.toLocaleString() == "essen,schlafen,studieren,prüfen,ausruhen" || testArr.toLocaleString() == "schlafen,essen,studieren,prüfen,ausruhen");