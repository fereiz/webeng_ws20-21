class Vorrang {
  constructor(vorrangPairs) {
    this.vorrangPairs = vorrangPairs;

    this.pairCount = vorrangPairs.length;

    //Gather unique activities
    this.activities = new Set([].concat(...vorrangPairs));

    //Set up an adjacency map for the "nodes"
    const adjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][0] === a) { arr.push(vorrangPairs[i][1]) }
      }
      adjMap.set(a, new Set(arr))
    }
    this.adjMap = adjMap;

    //Set up an inverse adjacency map for the "nodes"
    const invAdjMap = new Map();
    for (const a of this.activities) {
      let arr = [];
      for (var i = 0; i < this.pairCount; i++) {
        if (vorrangPairs[i][1] === a) { arr.push(vorrangPairs[i][0]) }
      }
      invAdjMap.set(a, new Set(arr))
    }
    this.invAdjMap = invAdjMap;
  }
}

const studentenLeben = new Vorrang([["schlafen", "studieren"], ["prüfen", "ausruhen"], ["essen", "studieren"], ["studieren", "prüfen"]]);

console.assert(studentenLeben.pairCount === 4);
console.assert(studentenLeben.activities.has("essen"));
console.assert(studentenLeben.activities.has("schlafen"));
console.assert(studentenLeben.activities.has("studieren"));
console.assert(studentenLeben.activities.has("prüfen"));
console.assert(studentenLeben.activities.has("ausruhen"));
console.assert(studentenLeben.adjMap.get("schlafen").has("studieren"));
console.assert(studentenLeben.invAdjMap.get("studieren").has("schlafen"));